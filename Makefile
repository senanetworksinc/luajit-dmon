#clang main.c -framework CoreFoundation -framework CoreServices -lpthread -o test

FILES := main.c

OBJECTS := main.o

LUA_VERSION = 5.1

PREFIX = /usr/local

LUA_CMODULE_DIR ?= $(PREFIX)/lib/lua/$(LUA_VERSION)

UNAME = $(shell uname -s)

ifeq ($(UNAME),Darwin)
	# clang
	CC := clang
	CFLAG := -framework CoreFoundation -framework CoreServices -lpthread
else
	# gnu
	CC := gcc
	CFLAG := -lpthread
endif

libdmon: $(OBJECTS)
	@echo "==== Linking ... ===="
	$(CC) -o $@.so -shared $(OBJECTS) $(CFLAG)

.c.o:
	@echo "==== C Building ... ===="
	$(CC) -fPIC -c $< -o $@ -std=gnu99

.PHONY: clean
clean:
	rm -f $(OBJECTS)
	rm -f libdmon.so

install:
	cp libdmon.so $(DESTDIR)$(LUA_CMODULE_DIR)/libdmon.so

uninstall:
	rm -f $(DESTDIR)$(LUA_CMODULE_DIR)/libdmon.so
