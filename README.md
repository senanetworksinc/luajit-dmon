# luajit-dmon

[dmon](https://github.com/septag/dmon)をSharedLibrary化したモジュールをLuajit FFIで呼び出すモジュールです。

こちらのソースがオリジナルです。

https://github.com/septag/dmon

# Install
`$ luarocks install luajit-dmon`

# API

`local watch = require "luajit-dmon.watch"`

## watch (luajit wrapper)

---

### .init()

watchインスタンスを生成して返す。

---

### .dmon_init()

dmonの初期化を行う。

`:watch()`を呼び出す前に本関数を呼び出す必要がある。

---

### .dmon_unwatch()

dmonの開放を行う。

dmonの使用を終える際に本関数を呼び出す必要がある。

---

### :watch(rootdir, callback, flags, user_data)

`rootdir`で指定されたディレクトリの監視を行う。

`flags`でオプションを指定する。

* watch.DMON_WATCHFLAGS_RECURSIVE
* watch.DMON_WATCHFLAGS_FOLLOW_SYMLINKS

`callback`で指定された関数にコールバックが行われる。

* `callback`引数 `(watch_id, action, rootdir, filepath, oldfilepath, user)`

* watch.DMON_ACTION_CREATE
* watch.DMON_ACTION_DELETE
* watch.DMON_ACTION_MODIFY
* watch.DMON_ACTION_MOVE

user_dataはユーザーデータが指定可能であるがffiにより適切に処理する必要がある。

---

### :unwatch()

`:watch()`で開始した監視処理を中断する。

# Usage

```lua
local watch = require "luajit-dmon.watch"

watch.dmon_init()

local watcher = watch.init()

local callback = function(watch_id, action, rootdir, filepath, oldfilepath, user)
	if action == watch.DMON_ACTION_CREATE then
		print(string.format("CREATE: [%s]%s\n", rootdir, filepath))
	elseif action == watch.DMON_ACTION_DELETE then
		print(string.format("DELETE: [%s]%s\n", rootdir, filepath))
	elseif action == watch.DMON_ACTION_MODIFY then
		print(string.format("MODIFY: [%s]%s\n", rootdir, filepath))
	elseif action == watch.DMON_ACTION_MOVE then
		print(string.format("MOVE: [%s]%s -> [%s]%s\n", rootdir, oldfilepath, rootdir, filepath))
	end
end

print("waiting for changes ..");

watcher:watch("./", callback, watch.DMON_WATCHFLAGS_RECURSIVE, nil)

io.read(1)

print("exit");

watcher:unwatch()

watch.dmon_unwatch()
```

# Revesion

* 2021/10/14 1.0-4 ffi.loadの読み込みパスを修正

* 2021/10/14 1.0-3 .soモジュールのインストール先修正

* 2021/10/14 1.0-2 Makefile コンパイルオプションを修正

* 2021/10/14 1.0-1 Makefile コンパイルオプションを修正

* 2021/10/01 1.0-0 release
