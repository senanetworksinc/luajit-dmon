local dmon = require "luajit-dmon.dmon"
local ffi = require "ffi"

local _M = {
	DMON_WATCHFLAGS_RECURSIVE = dmon.DMON_WATCHFLAGS_RECURSIVE,
	DMON_WATCHFLAGS_FOLLOW_SYMLINKS = dmon.DMON_WATCHFLAGS_FOLLOW_SYMLINKS,
	DMON_WATCHFLAGS_OUTOFSCOPE_LINKS = dmon.DMON_WATCHFLAGS_OUTOFSCOPE_LINKS,
	DMON_WATCHFLAGS_IGNORE_DIRECTORIES = dmon.DMON_WATCHFLAGS_IGNORE_DIRECTORIES,
	DMON_ACTION_CREATE = dmon.DMON_ACTION_CREATE,
	DMON_ACTION_DELETE = dmon.DMON_ACTION_DELETE,
	DMON_ACTION_MODIFY = dmon.DMON_ACTION_MODIFY,
	DMON_ACTION_MOVE = dmon.DMON_ACTION_MOVE,
}

_M.index = _M

---
--
--
function _M.init()
	local self = setmetatable(_M, {})

	self._dmon_watch_id = nil

	self._callback = nil

	return self
end

---
--
--
function _M.dmon_init()
	dmon.dmon_init()
end

---
--
--
function _M.dmon_unwatch()
	dmon.dmon_deinit()
end

---
--
--
function _M:watch(rootdir, callback, flags, user_data)
	if self._dmon_watch_id == nil then
		self._callback = ffi.cast("void (*)(uint32_t, uint32_t, const char*, const char*, const char*, void*)", function(watch_id, action, rootdir, filepath, oldfilepath, user)
			if filepath ~= nil then
				filepath = ffi.string(filepath)
			else
				filepath = nil
			end

			if oldfilepath ~= nil then
				oldfilepath = ffi.string(oldfilepath)
			else
				oldfilepath = nil
			end

			if rootdir ~= nil then
				rootdir = ffi.string(rootdir)
			else
				rootdir = nil
			end

			callback(watch_id, action, rootdir, filepath, oldfilepath, user)
		end)

		self._dmon_watch_id = dmon.dmon_watch(rootdir, self._callback, flags, user_data)
	end
end

---
--
--
function _M:unwatch()
	if self._callback ~= nil then
		self._callback:free()

		self._callback = nil
	end

	if self._dmon_watch_id ~= nil then
		dmon.dmon_unwatch(self._dmon_watch_id)

		self._dmon_watch_id = nil
	end
end


return _M