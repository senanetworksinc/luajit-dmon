local ffi = require "ffi"
local loaded, dmon = pcall(ffi.load, "dmon")

if not loaded then
	loaded, dmon = pcall(ffi.load, "libdmon.so")

	if not loaded then
		loaded, dmon = pcall(ffi.load, "/usr/local/lib/lua/5.1/libdmon.so")

		if not loaded then
			error("can't load dmon!")
		end
	end
end

ffi.cdef[[
typedef struct { uint32_t id; } dmon_watch_id;

// Pass these flags to `dmon_watch`
typedef enum dmon_watch_flags_t {
    DMON_WATCHFLAGS_RECURSIVE = 0x1,            // monitor all child directories
    DMON_WATCHFLAGS_FOLLOW_SYMLINKS = 0x2,      // resolve symlinks (linux only)
    DMON_WATCHFLAGS_OUTOFSCOPE_LINKS = 0x4,     // TODO: not implemented yet
    DMON_WATCHFLAGS_IGNORE_DIRECTORIES = 0x8    // TODO: not implemented yet
} dmon_watch_flags;

// Action is what operation performed on the file. this value is provided by watch callback
typedef enum dmon_action_t {
    DMON_ACTION_CREATE = 1,
    DMON_ACTION_DELETE,
    DMON_ACTION_MODIFY,
    DMON_ACTION_MOVE
} dmon_action;

extern void dmon_init(void);

extern void dmon_deinit(void);

extern  dmon_watch_id dmon_watch(const char* rootdir,
                         void (*watch_cb)(dmon_watch_id watch_id, dmon_action action,
                                          const char* rootdir, const char* filepath,
                                          const char* oldfilepath, void* user),
                         uint32_t flags, void* user_data);


extern void dmon_unwatch(dmon_watch_id id);
]]

return dmon